### Display the Exercise List

Displays a list of exercises and details of selected exercise.

#### Installation

Application dependencies should all be able to be pulled down via:

```bash
npm install
```

#### Running

Running the application using Webpack Dev Server can be done by running:

```bash
npm start
```

Building for production/hosting on an actual server can be done by running:

```bash
npm run build
```

The output bundle will be in the `./dist` directory.

#### Tests

```bash
npm test
```
