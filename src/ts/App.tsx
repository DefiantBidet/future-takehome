import { useContext, useEffect, useState } from 'react';

import { loadExercises } from 'api';
import DetailPanel from 'Components/DetailPanel';
import ExercisePanel from 'Components/ExercisePanel';
import LoadingOverlay from 'Components/LoadingOverlay';
import { ExerciseContext } from 'Contexts/ExerciseContext';

import styles from 'Styles/app.scss';

const appElement = document.querySelector('main#app');
const correlatedSVGsElement = document.querySelector('svg.correlated-svgs');

// Apply styles from App bundle to DOM - this is a webpack dev server issue
// and should not be needed but didn't wan't to waste time on bundling setup issues
appElement?.classList.add(styles.appContainer);
correlatedSVGsElement?.classList.add(styles.svgContainer);

/**
 * ExerciseLibrary is the main entry point of the Application to display
 * a list of exercises and select an exercise to view more information.
 * @return {JSX.Element}
 * @function
 */
export default function ExerciseLibrary(): JSX.Element {
  const [isLoading, setLoading] = useState<boolean>(true);
  const { setExerciseList } = useContext(ExerciseContext);

  // When the component mounts - fetch the exercise list
  useEffect(() => {
    const fetchExercises = async () => {
      const response = await loadExercises();

      if (response.data) {
        setLoading(false);
        setExerciseList(response.data);
      }
    };

    fetchExercises().catch(console.error);
  }, []);

  return (
    <>
      {isLoading && <LoadingOverlay />}
      <ExercisePanel />
      <DetailPanel />
    </>
  );
}
