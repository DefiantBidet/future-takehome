declare module 'Future' {
  export interface AudioData {
    url: string;
  }

  export interface VideoData {
    isFlipped: boolean;
    url: string;
  }

  export interface Exercise {
    audio: AudioData;
    description: string;
    equipmentRequired: string | null;
    id: string;
    isAlternating: boolean;
    movementPatterns: string;
    muscleGroups: string | null;
    name: string;
    side: string;
    synonyms: string;
    video: VideoData;
  }

  export interface PredictionSkillLevel {
    level: number;
    maxLevel: number;
    predictionConfidence: number;
  }

  export interface ExercisePrediction {
    exerciseId: string;
    predictedAt: string;
    skillLevel: PredictionSkillLevel;
    predictionTimeCostMilliseconds: number;
  }
}
