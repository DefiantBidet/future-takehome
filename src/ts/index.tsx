import ReactDOM from 'react-dom';

import App from './App';
import ExerciseProvider from 'Contexts/ExerciseContext';

const appElement = document.querySelector('main#app');

ReactDOM.render(
  <ExerciseProvider>
    <App />
  </ExerciseProvider>,
  appElement,
);
