import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ExerciseContext, IExerciseContext } from 'Contexts/ExerciseContext';
import { createMockExerciseList, createMockExerciseContext } from 'Test/utils';

import ExerciseList, { ExerciseListProps } from './index';



describe('<ExerciseList />', () => {
  const defaultListLength = 5;
  let mockProps: ExerciseListProps;

  const createMockProps = (mockProps?: Partial<ExerciseListProps>): ExerciseListProps => {
    return {
      list: mockProps?.list ?? createMockExerciseList(defaultListLength),
    };
  };

  beforeEach(() => {
    mockProps = createMockProps();
  });

  test('renders an unordered list container', () => {
    render(<ExerciseList {...mockProps} />);

    const element = document.querySelector('ul.listContainer');
    expect(element).toBeInTheDocument();
  });

  test('renders anchor elements', () => {
    render(<ExerciseList {...mockProps} />);

    const anchorElements = document.querySelectorAll('a');
    expect(anchorElements.length).toBe(defaultListLength);
  });

  test('renders list item elements', () => {
    render(<ExerciseList {...mockProps} />);

    const listItems = document.querySelectorAll('li');
    expect(listItems.length).toBe(defaultListLength);
  });

  test('renders an empty list', () => {
    mockProps = createMockProps({ list: [] });

    render(<ExerciseList {...mockProps} />);

    const listItems = document.querySelectorAll('li');
    expect(listItems.length).toBe(0);
  });

  test('renders a list with no selection', () => {
    const mockExerciseContext: IExerciseContext = createMockExerciseContext();

    render(
      <>
        <ExerciseContext.Provider value={mockExerciseContext}>
          <ExerciseList {...mockProps} />
        </ExerciseContext.Provider>
      </>
    );

    const listItemElements = document.querySelectorAll('li.listItem');
    expect(listItemElements.length).toBe(defaultListLength);
  });

  test('renders a list with a selection', () => {
    const mockSelectedExercise = mockProps.list[0];
    const mockExerciseContext: IExerciseContext = createMockExerciseContext({ selectedExercise: mockSelectedExercise });

    render(
      <>
        <ExerciseContext.Provider value={mockExerciseContext}>
          <ExerciseList {...mockProps} />
        </ExerciseContext.Provider>
      </>,
    );

    const selectedListItemElement = document.querySelector('li.listItem--selected');
    const unselectedListItemElements = document.querySelectorAll('li.listItem');

    expect(selectedListItemElement).toBeInTheDocument();
    expect(unselectedListItemElements.length).toBe(defaultListLength - 1);
  });

  test('selecting an item calls Context method to select exercise', () => {
    const mockSelectedExercise = mockProps.list[0];
    const contextSpy = jest.fn();
    const mockExerciseContext: IExerciseContext = createMockExerciseContext({ setSelectedExercise: contextSpy });

    render(
      <>
        <ExerciseContext.Provider value={mockExerciseContext}>
          <ExerciseList {...mockProps} />
        </ExerciseContext.Provider>
      </>,
    );

    const selectedExercise = document.querySelector(
      `a[data-exercise-id="${mockSelectedExercise.id}"]`,
    );

    expect(selectedExercise).toBeInTheDocument();

    userEvent.click(selectedExercise!);

    expect(contextSpy).toHaveBeenCalledWith(mockSelectedExercise);
  });
});
