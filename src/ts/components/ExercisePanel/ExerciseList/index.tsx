import { useContext } from 'react';

import { ExerciseContext } from 'Contexts/ExerciseContext';

import * as FutureAPI from 'Future';

import styles from 'Styles/components/exerciseList.scss';

/**
 * Props for ExerciseList
 */
export interface ExerciseListProps {
  list: FutureAPI.Exercise[];
}

/**
 * ExerciseList renders a list of exercises.
 * @param {ExerciseListProps} props  Props of the Component
 * @return {JSX.Element}
 * @function
 */
export default function ExerciseList(props: ExerciseListProps): JSX.Element {
  const { selectedExercise, setSelectedExercise } = useContext(ExerciseContext);

  /**
   * Click handler for selecting the anchor element of the unordered list
   * @param  {React.MouseEvent<HTMLInputElement>} event MouseEvent of the anchor element
   * @return {void}
   * @function
   */
  const onSelectExercise = (event: React.MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    const exerciseId = event.currentTarget.getAttribute('data-exercise-id');
    if (exerciseId) {
      const exercise = props.list.find(
        (exercise: FutureAPI.Exercise) => exercise.id === exerciseId,
      );
      if (exercise) {
        setSelectedExercise(exercise);
      }
    }
  };

  return (
    <>
      <ul className={styles.listContainer}>
        {props.list.map((exercise: FutureAPI.Exercise, index: number) => {
          const className =
            selectedExercise?.id === exercise.id ? styles['listItem--selected'] : styles.listItem;
          return (
            <a
              href="#"
              className={styles.exerciseLink}
              onClick={onSelectExercise}
              key={`exercise-${index}`}
              data-exercise-id={exercise.id}
            >
              <li className={className}>{exercise.name}</li>
            </a>
          );
        })}
      </ul>
    </>
  );
}
