import { useContext, useEffect, useState } from 'react';

import ExerciseList from 'Components/ExercisePanel/ExerciseList';
import TextInput from 'Components/ExercisePanel/TextInput';
import { ExerciseContext } from 'Contexts/ExerciseContext';

import * as FutureAPI from 'Future';

import styles from 'Styles/components/exercisePanel.scss';

/**
 * ExercisePanel renders a panel to display a list of exercises
 * @return {JSX.Element}
 * @function
 */
export default function ExercisePanel(): JSX.Element {
  const { exerciseList } = useContext(ExerciseContext);
  const [list, setList] = useState<FutureAPI.Exercise[]>([]);

  // when the component mounts set the current state `list` to the context value
  // dependency on the context value
  useEffect(() => {
    setList(exerciseList);
  }, [exerciseList]);

  /**
   * Input change handler
   * @param  {string} value Input value
   * @return {void}
   * @function
   */
  const onInputChange = (value: string): void => {
    if (value.length > 0) {
      const filteredList = list.filter((exercise: FutureAPI.Exercise) => exercise.name.toLowerCase().includes(value.toLowerCase()));
      setList(filteredList);
      return;
    }
    // revert to default
    setList(exerciseList);
  };

  return (
    <>
      <div className={styles.exerciseContainer}>
        <div className={styles.filterContainer}>
          <TextInput
            elementName="filter-exercises"
            placeHolder="Filter Exercises"
            onInput={onInputChange}
          />
        </div>
        <div className={styles.listContainer}>
          <ExerciseList list={list} />
        </div>
      </div>
    </>
  );
}
