import { useState } from 'react';

import styles from 'Styles/components/textInput.scss';

/**
 * Props for TextInput
 */
export interface TextInputProps {
  elementName: string;
  inputValue?: string;
  onInput?: (value: string) => void;
  placeHolder: string;
};

/**
 * TextInput renders a text input field
 * @param {TextInputProps} props Props of Component
 * @return {JSX.Element}
 * @function
 */
export default function TextInput(props: TextInputProps): JSX.Element {
  const [inputValue, setInputValue] = useState<string>(props.inputValue ?? '');
  const { elementName, placeHolder } = props;

  /**
   * onBlur event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Blur Event
   * @return {void}
   * @function
   */
  const onBlur = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    event.preventDefault();

    const trimmedValue = inputValue.trim();
    // handle trimmed string being different from supplied
    if (inputValue !== trimmedValue) {
      setInputValue(trimmedValue);
    }
  };

  /**
   * onInput event handler
   * @param  {React.ChangeEvent<HTMLInputElement>} event Input Event
   * @return {void}
   * @function
   */
  const onInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    event.preventDefault();

    const { value } = event.target;

    setInputValue(value);

    if (props.onInput) {
      props.onInput(value);
    }
  };

  return (
    <>
      <input
        aria-required={true}
        aria-label={placeHolder}
        id={elementName}
        className={styles.input}
        name={elementName}
        onBlur={onBlur}
        onInput={onInput}
        placeholder={placeHolder}
        type="text"
        value={inputValue}
      />
    </>
  );
};
