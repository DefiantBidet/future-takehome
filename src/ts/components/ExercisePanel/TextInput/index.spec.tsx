import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import TextInput, { TextInputProps } from "./index";

describe('<TextInput />', () => {
  let mockProps: TextInputProps;

  const createMockProps = (mockProps?: Partial<TextInputProps>): TextInputProps => {
    return {
      elementName: mockProps?.elementName ?? 'foo',
      placeHolder: mockProps?.placeHolder ?? 'Enter Foo',
      ...mockProps,
    };
  };

  beforeEach(() => {
    jest.clearAllMocks();
    mockProps = createMockProps();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('creates input', () => {
    render(<TextInput {...mockProps} />);
    const inputElement = document.querySelector(`#${mockProps.elementName}`);
    expect(inputElement).toBeInTheDocument();
  });

  test('creates placeholder label', () => {
    render(<TextInput {...mockProps} />);
    const placeholderLabel = screen.getByLabelText(mockProps.placeHolder);
    expect(placeholderLabel).toBeInTheDocument();
  });

  test('calls supplied onInput handler', () => {
    const mockText = 'foo';
    const mockInputHandler = jest.fn();
    mockProps.onInput = mockInputHandler;

    render(<TextInput {...mockProps} />);
    const inputElement: HTMLInputElement = screen.getByRole('textbox', {
      name: mockProps.placeHolder,
    });
    userEvent.type(inputElement, mockText);

    expect(mockInputHandler).toBeCalledWith(mockText);
  });
});
