import { useContext, useEffect, useState } from 'react';

import { loadExercisePredictionData } from 'Api/index';
import SkillLevel from 'Components/DetailPanel/SkillLevel';
import { ExerciseContext } from 'Contexts/ExerciseContext';
import * as FutureAPI from 'Future';

import styles from 'Styles/components/detailsPanel.scss';

/**
 * DetailPanel renders the details of the selected exercise
 * @param {DetailPanelProps} props  Props of the Component
 * @return {JSX.Element}
 * @function
 */
export default function DetailPanel(): JSX.Element {
  const [isLoading, setLoading] = useState<boolean>(true);
  const [exercisePredictionData, setExercisePredictionData] =
    useState<FutureAPI.ExercisePrediction | null>(null);

  const { selectedExercise } = useContext(ExerciseContext);

  // when the component mounts fetch the machine learning data based on selected exercise id
  useEffect(() => {
    if (!selectedExercise) return;

    const fetchExercisePredictionData = async () => {
      const response = await loadExercisePredictionData(selectedExercise.id);

      if (response.data) {
        setLoading(false);
        setExercisePredictionData(response.data);
      }
    };

    fetchExercisePredictionData().catch(console.error);
  }, [selectedExercise]);

  /**
   * Renders the Empty Details Pane - No Selected Exercise
   * @return {JSX.Element}
   * @function
   */
  const emptySelection = (): JSX.Element => {
    return (
      <div className={styles['detailsContainer--empty']}>
        Please select an exercise to view more information.
      </div>
    );
  };

  /**
   * Renders the Populated Details Pane
   * @return {JSX.Element}
   * @function
   */
  const showDetails = (): JSX.Element => {
    return (
      <>
        <div className={styles.detailsContainer}>
          <h1 className={styles.exerciseHeader}>{selectedExercise!.name}</h1>
          <p className={styles.exerciseDescription}>{selectedExercise!.description}</p>
          {!isLoading && (
            <SkillLevel predictionData={exercisePredictionData} exercise={selectedExercise} />
          )}
          {selectedExercise!.muscleGroups && <p>Muscle Groups: {selectedExercise!.muscleGroups}</p>}
          {selectedExercise!.equipmentRequired && (
            <p>Equipment Required: {selectedExercise!.equipmentRequired}</p>
          )}
          {selectedExercise!.movementPatterns && (
            <p>Movement Patterns: {selectedExercise!.movementPatterns}</p>
          )}
          {selectedExercise!.synonyms && <p>Synonyms: {selectedExercise!.synonyms}</p>}
          <p>Alternating: {selectedExercise!.isAlternating.toString()}</p>
          {selectedExercise!.side && <p>Side: {selectedExercise!.side}</p>}
          {selectedExercise!.video && (
            <video controls key={selectedExercise!.video.url}>
              <source src={selectedExercise!.video.url} type="video/mp4" />
              This browser doesn't support video tag.
            </video>
          )}
        </div>
      </>
    );
  };

  return selectedExercise === null ? emptySelection() : showDetails();
}
