import { render, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import { ExerciseContext, IExerciseContext } from 'Contexts/ExerciseContext';
import { createMockExercise, createMockExerciseContext } from 'Test/utils';

import DetailPanel from './index';

describe('<DetailPanel />', () => {
  test('renders an empty list container', async () => {
    const mockExerciseContext: IExerciseContext = createMockExerciseContext();

    await act(async () => {
      render(
        <>
          <ExerciseContext.Provider value={mockExerciseContext}>
            <DetailPanel />
          </ExerciseContext.Provider>
        </>,
      );
    });


    waitFor(() => {
      const element = document.querySelector('div.detailsContainer--empty');
      expect(element).toBeInTheDocument();
    });
  });

  test('renders a details container', async () => {
    const mockSelectedExercise = createMockExercise();
    const mockExerciseContext: IExerciseContext = createMockExerciseContext({
      selectedExercise: mockSelectedExercise,
    });


    await act(async () => {
      render(
        <>
          <ExerciseContext.Provider value={mockExerciseContext}>
            <DetailPanel />
          </ExerciseContext.Provider>
        </>,
      );
    });

    waitFor(() => {
      const element = document.querySelector('div.detailsContainer');
      expect(element).toBeInTheDocument();
    });
  });
});
