import { render, screen } from '@testing-library/react';

import SkillLevel, { SkillLevelProps } from './index';

import { createMockExercise, createMockExercisePrediction } from 'Test/utils';

describe('<SkillLevel />', () => {
  const mockProps: SkillLevelProps = {
    exercise: createMockExercise(),
    predictionData: createMockExercisePrediction(),
  };

  test('creates a skill level container', () => {
    render(<SkillLevel {...mockProps} />);

    const element = document.querySelector('div.skillsContainer');
    expect(element).toBeInTheDocument();
  });

  test('has `Difficulty` label', () => {
    const expectedText = 'Difficulty:';

    render(<SkillLevel {...mockProps} />);

    const content = screen.getByText(expectedText);
    expect(content).toBeInTheDocument();
  });

  test('creates span container of icons', () => {
    render(<SkillLevel {...mockProps} />);

    const element = document.querySelector('span.skillsIconsContainer');
    expect(element).toBeInTheDocument();
  })

  test('icon container has expected number children', () => {
    render(<SkillLevel {...mockProps} />);

    const icons = screen.getAllByRole('img');
    expect(icons.length).toBe(mockProps.predictionData?.skillLevel.maxLevel);
  });

  test('icon container has expected filled icons', () => {
    render(<SkillLevel {...mockProps} />);

    const icons = document.querySelectorAll('use[href="#circle-icon-filled"]');
    expect(icons.length).toBe(mockProps.predictionData?.skillLevel.level);
  });

  test('icon container has expected outlined icons', () => {
    const iconCount = mockProps.predictionData!.skillLevel.maxLevel - mockProps.predictionData!.skillLevel.level;

    render(<SkillLevel {...mockProps} />);

    const icons = document.querySelectorAll('use[href="#circle-icon-outline"]');
    expect(icons.length).toBe(iconCount);
  });
});
