
import * as FutureAPI from 'Future';

import styles from 'Styles/components/skillLevel.scss';

/**
 * Props for SkillLevel
 */
export interface SkillLevelProps {
  exercise: FutureAPI.Exercise | null;
  predictionData: FutureAPI.ExercisePrediction | null;
}

/**
 * SkillLevel renders an indication of skill level of exercise.
 * Displayed information shows skill level out of maximum skill level.
 * (e.g. 3 out of 5 would be displayed as `***oo`)
 * @param {SkillLevelProps} props  Props of the Component
 * @return {JSX.Element}
 * @function
 */
export default function SkillLevel(props: SkillLevelProps): JSX.Element | null {
  if (!props.predictionData) return null;

  const currentLevel = props.predictionData.skillLevel.level;
  const maxLevel = props.predictionData.skillLevel.maxLevel;

  // filled icon is the current skill level indicated
  const filledIconsCount = currentLevel;

  // outline icons indicate total possible skill levels
  const outlineIconsCount = Math.floor(maxLevel - currentLevel);

  /**
   * Renders the SVG Icon indicating the current skill level
   * @return {JSX.Element}
   * @function
   */
  const renderIcon = (index: number, filled: boolean) => {
    if (!props.exercise) return null;
    const key = filled
      ? `${props.exercise.id}-skill-icon-filled-${index}`
      : `${props.exercise.id}-skill-icon-outline-${index}`;
    const iconName = filled ? '#circle-icon-filled' : '#circle-icon-outline';

    return (
      <span key={key} className={styles.skillLevel}>
        <svg role="img" aria-label="skill level">
          <use href={iconName}></use>
        </svg>
      </span>
    );
  };

  return (
    <>
      <div className={styles.skillsContainer}>
        Difficulty:
        <span className={styles.skillsIconsContainer}>
          {Array.from({ length: filledIconsCount }, (_, i) => i).map((index) =>
            renderIcon(index, true),
          )}
          {Array.from({ length: outlineIconsCount }, (_, i) => i).map((index) =>
            renderIcon(index, false),
          )}
        </span>
      </div>
    </>
  );
}
