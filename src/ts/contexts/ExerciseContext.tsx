import React, { createContext, useState } from 'react';

import * as FutureAPI from 'Future';

const noop = () => { /* noop */ };

export interface IExerciseContext {
  exerciseList: FutureAPI.Exercise[];
  setExerciseList: (exerciseList: FutureAPI.Exercise[]) => void;
  selectedExercise: FutureAPI.Exercise | null;
  setSelectedExercise: (exercise: FutureAPI.Exercise) => void;
}

const initialContext = {
  exerciseList: [],
  setExerciseList: noop,
  selectedExercise: null,
  setSelectedExercise: noop,
};

export const ExerciseContext = createContext<IExerciseContext>(initialContext);

const ExerciseProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [exerciseList, setExerciseList] = useState<FutureAPI.Exercise[]>([]);
  const [selectedExercise, setSelectedExercise] = useState<FutureAPI.Exercise | null>(null);

  const providerValues: IExerciseContext = {
    exerciseList,
    setExerciseList,
    selectedExercise,
    setSelectedExercise,
  };

  return <ExerciseContext.Provider value={providerValues}>{children}</ExerciseContext.Provider>;
};

export default ExerciseProvider;
